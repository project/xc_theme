<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html lang="<?php print $language->language ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  
  <body class="<?php print $body_classes ?>">
    
    <!-- Page -->
    <div id="page" class="clear-block">
      <div id="page-inner">
        
        <!-- Main -->
        <div id="main" class="clear-block">
          <div id="main-inner">
              
              <!-- Top -->
              <div id="top" class="clear-block">
                <div id="top-inner">
                  <?php print $top ?>
                  
                  <!-- Header -->
                  <div id="header" class="clear-block">
                    <div id="header-inner">
                      <?php if ($logo): ?>
                        <a href="<?php print $front_page ?>" title="<?php print t('Home') ?>" rel="home" id="logo">
                          <img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" />
                        </a>
                      <?php endif ?>

                      <!-- Site name, slogan, and other information -->
                      <?php if ($site_name || $site_slogan): ?>
                        <div id="site-information">
                          <?php if ($site_name): ?>
                            <h1 id="site-name">
                              <a href="<?php print $front_page ?>" title="<?php print t('Home') ?>" rel="home">
                                <span><?php print $site_name ?></span>
                              </a>
                            </h1>
                          <?php endif ?>

                          <?php if ($site_slogan): ?>
                            <div id="site-slogan"><?php print $site_slogan ?></div>
                          <?php endif ?>
                        </div>
                      <?php endif ?>

                      <!-- Header, primary, and secondary links -->
                      <?php if ($header_links || $xc_primary_links || $xc_secondary_links || $primary_links || $secondary_links): ?>
                        <div id="header-links">
                          <div id="header-links-secondary">
                            <?php if ($xc_secondary_links): ?>
                              <div id="xc-secondary">
                                <?php print theme('links', $xc_secondary_links); ?>
                              </div>
                            <?php endif; ?>

                            <?php if ($secondary_links): ?>
                              <div id="secondary">
                                <?php print theme('links', $secondary_links); ?>
                              </div>
                            <?php endif; ?>
                          </div>
                          <div id="header-links-primary">
                            <?php if ($xc_primary_links): ?>
                              <div id="xc-primary">
                                <?php print theme('xc_primary_links', $xc_primary_links, $active_xc_primary_link); ?>
                              </div>
                            <?php endif ?>

                            <?php if ($primary_links): ?>
                              <div id="primary">
                                <?php print theme('links', $primary_links); ?>
                              </div>
                            <?php endif; ?>
                          </div>

                          <?php print $header_links ?>
                        </div>
                      <?php endif ?>


                      <?php print $header ?>
                    </div>
                  </div>
                  
                </div>
                 <!-- Breadcrumb -->
                 <?php print $breadcrumb ?>
              </div>
              

              <!-- Middle -->
              <div id="middle" class="clear-block">
                <div id="middle-inner">

                  <!-- Tabs -->
                  <?php if ($tabs): ?>
                    <div class="tabs"><?php print $tabs ?></div>
                  <?php endif ?>

                  <!-- Left sidebar -->
                  <div id="left" class="sidebar clear">
                    <div id="left-inner">
                      <?php print $left ?>
                    </div>
                  </div>
              
                  <!-- Center container -->
                  <div id="center" class="container">
                    <div id="center-inner">

                      <!-- Content top -->
                      <?php if ($content_top || $title || $messages || $help): ?>
                        <div id="content-top">
                          <div id="content-top-inner">
                            <?php if ($title): ?>
                              <h1 class="title"><?php print $title ?></h1>
                            <?php endif ?>

                            <?php print $content_top ?>
                            <?php print $messages ?>
                            <?php print $help ?>
                          </div>
                        </div>
                      <?php endif ?>

                      <!-- Content -->
                      <?php if ($content): ?>

                      <div id="content">
                        <div id="content-inner">
                          <?php print $content ?>
                        </div>
                      </div>

                      <?php endif; ?>
                  
                      <!-- Content bottom -->
                      <?php if ($feed_icons || $content_bottom): ?>
                        <div id="content-bottom">
                          <div id="content-bottom-inner">
                            <?php if ($feed_icons): ?>
                              <div class="feed-icons"><?php print $feed_icons ?></div>
                            <?php endif ?>

                            <?php print $content_bottom ?>
                          </div>
                        </div>
                      <?php endif ?>
              
                    </div>
                  </div>
              
                  <!-- Right sidebar -->
                  <div id="right" class="sidebar">
                    <div id="right-inner">    

                      <!-- Site mission -->
                      <?php if ($mission): ?>
                        <div id="mission"><?php print $mission ?></div>
                      <?php endif ?>

                      <?php print $right ?>
                    </div>
                  </div>

                  <!-- Clearing -->
                  <div class="clear"></div>

                </div>
              </div>
              
              <!-- Bottom -->
              <div id="bottom" class="clear-block clear">
                <div id="bottom-inner">
                  
                  <!-- Footer -->
                  <div id="footer" class="clear-block">
                    <div id="footer-inner">
                      <?php print $footer ?>
                    </div>
                  </div>
                  
                  <?php print $bottom ?>
                </div>
              </div>
        
          </div>
        </div>  
        
      </div>
    </div>
    
    <!-- Closure -->
    <?php print $closure ?>
  </body>
</html>