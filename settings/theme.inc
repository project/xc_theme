<?php

function xc_theme_get_default_settings($theme = 'xc_theme') {
  global $theme_key;
  
  $themes = list_themes();

  // Get default values
  $defaults = !empty($themes[$theme]->info['settings']) ? $themes[$theme]->info['settings'] : array();
  
  // Set new values for settings
  if (!empty($defaults)) {
    $settings = array_merge($defaults, variable_get('xc_theme_'. $theme .'_settings', array()));
    variable_set('xc_theme_'. $theme .'_settings', $settings);
    
    // Refresh if necessary
    if (!empty($theme_key)) {
      theme_get_setting('', TRUE);
    }
  }

  return $defaults;
}
