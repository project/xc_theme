<?php

function phptemplate_preprocess_page(&$variables) {
  global $user;

  // Creating arrays
  $xc_primary_links = array();
  $xc_secondary_links = array();

  // Add site mission to right sidebar
  if ($variables['mission'] && empty($variables['right'])) {
    $variables['right'] = '&nbsp;';
  }

  // Get body classes
  $body_classes = explode(' ', $variables['body_classes']);

  // Clean up body classes
  unset($body_classes[array_search('no-sidebars', $body_classes)]);
  unset($body_classes[array_search('one-sidebar', $body_classes)]);
  unset($body_classes[array_search('two-sidebars', $body_classes)]);
  unset($body_classes[array_search('sidebar-left', $body_classes)]);
  unset($body_classes[array_search('sidebar-right', $body_classes)]);

  // Fix layout
  $variables['layout'] = 'none';
  if (!empty($variables['left'])) {
    $variables['layout'] = 'left';
  }
  if (!empty($variables['right'])) {
    $variables['layout'] = ($variables['layout'] == 'left') ? 'both' : 'right';
  }

  // Fix information about the number of sidebars.
  if ($variables['layout'] == 'both') {
    $body_classes[] = 'two-sidebars';
  }
  elseif ($variables['layout'] == 'none') {
    $body_classes[] = 'no-sidebars';
  }
  else {
    $body_classes[] = 'one-sidebar sidebar-'. $variables['layout'];
  }
  
  // Add breadcrumb to body classes
  if ($variables['breadcrumb']) {
    $body_classes[] = 'has-breadcrumb';
  }

  $xc_title_settings = theme_get_setting('xc_title');

  if ($xc_title_settings['show'] == 'never') {
    $body_classes[] = 'hide-title';
  }

  // Advanced theming body classes
  if (!$variables['is_front']) {
    $path = drupal_get_normal_path($_GET['q']);

    // For each normal page
    $body_classes[] = 'page-' . xc_normalize_path($path);

    // For XC sections
    if (arg(0) == 'admin' && arg(1) == 'xc') {
      $body_classes[] = 'admin-xc';
      if ($path != 'admin/xc') {
        $pos = strpos($path, '/', 9);
        $path_input = ($pos !== FALSE) ? substr($path, 0, $pos) : $path;
        $normalized_path = xc_normalize_path($path_input);
        if (!in_array($normalized_path, $body_classes)) {
          $body_classes[] = $normalized_path;
        }
      }
    }
    else if (arg(0) == 'xc' && $path != 'xc') {
      $pos = strpos($path, '/', 3);
      $path_input = ($pos !== FALSE) ? substr($path, 0, $pos) : $path;
      $normalized_path = xc_normalize_path($path_input);
      if (!in_array($normalized_path, $body_classes)) {
        $body_classes[] = $normalized_path;
      }
    }
    else if (arg(0) == 'user' && in_array(arg(2), array('loaned-items', 'requested-items', 'bookmarked-items'))) {
      $body_classes[] = 'user-' . arg(2);
    }
  }

  // Header links settings
  $xc_header_links_settings = theme_get_setting('xc_header_links');

  if ($xc_header_links_settings['home']) {
    $xc_primary_links[] = array(
      'title' => t('Home'),
      'href' => '<front>',
    );
  }

  if ($xc_header_links_settings['login_logout']) {
    if (user_is_logged_in()) {
      $xc_secondary_links[] = array(
        'title' => '<span class="xc-logged-in-text">' . t('Logged in: ') . '</span>' . theme('username', $user),
        'html' => TRUE
      );
    }
    else {
      $xc_secondary_links[] = array(
        'title' => t('Login'),
        'href' => 'xc/login',
        'query' => 'x-' . drupal_get_destination()
      );
    }
  }

  if ($user->uid && $xc_header_links_settings['my_account']) {
    $xc_secondary_links[] = array(
      'title' => t('My Account'),
      'href' => 'user',
    );
  }

  if ($xc_header_links_settings['login_logout'] && user_is_logged_in()) {
    $xc_secondary_links[] = array(
      'title' => t('Logout'),
      'href' => 'xc/logout',
      'query' => drupal_get_destination(),
    );
  }

  // Search settings
  if (module_exists('xc_search')) {
    $xc_search_settings = theme_get_setting('xc_search');

    // Hide title from search page
    if ($xc_search_settings['hide_title'] && is_xc_search_path()) {
      unset($variables['title']);
    }

    // Hide title from full record page
    if ($xc_search_settings['hide_full_record_title'] && is_metadata_node($variables['node'])) {
      unset($variables['title']);
    }
  }

  // Browse settings
  if (module_exists('xc_browse')) {
    $xc_browse_settings = theme_get_setting('xc_browse');

    // Show browse pages
    if ($xc_browse_settings['show_pages']) {
      $uis = xc_browse_ui_get_all();
      foreach ($uis as $ui){
        $xc_primary_links[] = array(
          'title' => $ui->label,
          'href' => 'xc_browse/' . $ui->name
        );
      }
    }
  }

  // Cleanup body classes
  foreach ($body_classes as &$body_class) {
    while (substr($body_class, -1) == '-') {
      $body_class = substr($body_class, 0, strlen($body_class) - 1);
    }
  }

  // Write variables
  $variables['body_classes'] = implode(' ', array_unique($body_classes));
  $variables['xc_primary_links'] = $xc_primary_links;
  $variables['xc_secondary_links'] = $xc_secondary_links;
}

function phptemplate_breadcrumb($breadcrumb) {
  $xc_breadcrumb_settings = theme_get_setting('xc_breadcrumb');

  // Rebuild entire breadcrumb
  if ($xc_breadcrumb_settings['rebuild']) { /* For now this is rebuilt by default */}

  $breadcrumb = array();
  
  // FIXME: Near copy of the menu_get_active_breadcrumb() with additional
  // adding of classes for breadcrumb links... may probably be easier to use
  // theme_item_list() to theme the breadcrumb array, but eventually XC will
  // rebuild menus as desired AND because this method allows use of the
  // "separator". Overall, this may need some more thought.
  if (!drupal_is_front_page()) {
    $item = menu_get_item();

    if ($item && $item['access']) {
      $active_trail = menu_get_active_trail();

      $count = 1;
      foreach ($active_trail as $parent) {
        $breadcrumb[] = l($parent['title'], $parent['href'], array(
          'attributes' => array(
            'class' => 'crumb' . ' crumb-' . $count . ' '
                     . ($count == 1 ? ' first' : '')
                     . ($count == count($active_trail) - 1 ? ' last' : '')
        )) + $parent['localized_options']);
        $count++;
      }
      $end = end($active_trail);

      // Don't show a link to the current page in the breadcrumb trail.
      if ($item['href'] == $end['href'] || ($item['type'] == MENU_DEFAULT_LOCAL_TASK && $end['href'] != '<front>')) {
        array_pop($breadcrumb);
      }
    }
  }

  if ($breadcrumb && ($xc_breadcrumb_settings['show'] == 'always' ||
     ($xc_breadcrumb_settings['show'] == 'admin' && arg(0) == 'admin'))) {

    // Separator
    $separator = $xc_breadcrumb_settings['separator']
               ? $xc_breadcrumb_settings['separator']
               : '';

    // Title
    $title_span = $xc_breadcrumb_settings['show_title'] && ($title = drupal_get_title())
                ? "<span>$title</span>"
                : '';

    return '<div class="breadcrumb">' . implode($separator, $breadcrumb) . "$title_span</div>";
  }
  
  return '';
}

function xc_normalize_path($path) {
  return strtolower(
      preg_replace('/(_|-)+/', '-',
        preg_replace('/[^A-Za-z0-9_-]+/', '', 
          str_replace('/', '-', $path))));
}

/**
 * Implementation of hook_theme().
 */
function xc_theme_theme($existing, $type, $theme, $path) {
  return array(
    'xc_primary_links' => array(
      'arguments' => array(
        'links' => array(), 
        'active_link' => NULL,
        'attributes' => array(),
      ),
    ),
  );
}

/**
 * Modified version of theme_link(). The main difference is how it find out if a link is 'active'.
 * It take active a link if the current path starts with link's href attribute. Retruns an unordered
 * list of links
 * 
 * @param $links (Array)
 *   Array of items, which may contain title and href keys.
 * @param $attributes (Array)
 *   HTML attributes
 * 
 * @return (String)
 *   Themed links
 */
function xc_theme_xc_primary_links($links, $active_link = '<front>', $attributes = array('class' => 'links')) {
  global $language;
  $output = '';
  if ($active_link != '<front>'
      && $active_link == drupal_get_normal_path(variable_get('site_frontpage', 'node'))) {
    $active_link = '<front>';
  }
  if (count($links) > 0) {
    $output = '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    $has_active = FALSE;
    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) 
          && !$has_active
          && (   $active_link == $link['href']
              || strpos($_GET['q'], $link['href']) === 0  
              || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) 
              || $link['language']->language == $language->language)) {
        $class .= ' active';
        $link['attributes'] = array('class' => 'active');
        $has_active = TRUE;
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}