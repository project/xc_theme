<?php

// Get default settings file
include_once './' . drupal_get_path('theme', 'xc_theme') . '/settings/theme.inc';

function xc_theme_settings($saved_settings, $subtheme_defaults = array()) {
  // Get default settings
  $defaults = array_merge(xc_theme_get_default_settings('xc_theme'), $subtheme_defaults);

  // Merge saved and default settings
  $settings = array_merge($defaults, $saved_settings);

  $form['xc_title'] = array(
    '#type' => 'fieldset',
    '#title' => t('Title'),
    '#description' => t('Configure title display'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  $form['xc_title']['show'] = array(
    '#type' => 'select',
    '#title' => t('Show'),
    '#default_value' => $settings['xc_breadcrumb']['show'],
    '#options' => array(
      'always' => t('Always'),
      'never' => t('Never')
    ),
  );

  $form['xc_breadcrumb'] = array(
    '#type' => 'fieldset',
    '#title' => t('Breadcrumb'),
    '#description' => t('Configure breadcrumb display options.'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  $form['xc_breadcrumb']['show'] = array(
    '#type' => 'select',
    '#title' => t('Show'),
    '#default_value' => $settings['xc_breadcrumb']['show'],
    '#options' => array(
      'always' => t('Always'),
      'never' => t('Never'),
      'admin' => t('Only for administration')),
  );

  $form['xc_breadcrumb']['separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Separator'),
    '#description' => t('Text to separate breadcumb links; leave blank for none'),
    '#default_value' => $settings['xc_breadcrumb']['separator'],
    '#size' => 5,
    '#maxlength' => 10,
  );

  $form['xc_breadcrumb']['show_home'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show home link in breadcrumb'),
    '#description' => t('Enable or disable displaying the home page link at the
                         beginning of the breadcrumb'),
    '#default_value' => $settings['xc_breadcrumb']['show_home'],
  );

  $form['xc_breadcrumb']['show_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show title in breadcrumb'),
    '#description' => t('Enable or disable displaying the title at the end of
                         the breadcrumb'),
    '#default_value' => $settings['xc_breadcrumb']['show_title'],
  );

  $form['xc_header_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header Links'),
    '#description' => t('Configure links to display within the header section.'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  $form['xc_header_links']['home'] = array(
    '#type' => 'checkbox',
    '#title' => t('Home'),
    '#default_value' => $settings['xc_header_links']['home'],
  );

  $form['xc_header_links']['login_logout'] = array(
    '#type' => 'checkbox',
    '#title' => t('Login / Logout'),
    '#default_value' => $settings['xc_header_links']['login_logout'],
  );

  $form['xc_header_links']['my_account'] = array(
    '#type' => 'checkbox',
    '#title' => t('My Account'),
    '#default_value' => $settings['xc_header_links']['my_account'],
  );

  $form['xc_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
    '#description' => t('Configure search display options.'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  $form['xc_search']['hide_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide page title'),
    '#description' => t('The page title may interfere with the look and feel
                         of the search interface.'),
    '#default_value' => $settings['xc_search']['hide_title']
  );

  $form['xc_search']['hide_full_record_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide full record page title'),
    '#description' => t('The node title may interfere with the look and feel
                         of the full record page.'),
    '#default_value' => $settings['xc_search']['hide_title']
  );

  $form['xc_browse'] = array(
    '#type' => 'fieldset',
    '#title' => t('Browse Pages'),
    '#description' => t('Configure browse pages options.'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  $form['xc_browse']['show_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show pages as tabs'),
    '#description' => t('Displays the browse pages as primary tabs.'),
    '#default_value' => $settings['xc_browse']['show_pages']
  );

  return $form;
}