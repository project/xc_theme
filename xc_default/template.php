<?php

function xc_default_xc_search_block_label($field_label, $label, $is_active) {
  if ($is_active) {
    return ( $field_label ? '<span class="xc-search-facet-field-label">' . $field_label . ': </span>' : '' )
    . '<span class="xc-search-facet-label">' . $label . '</span>';
  }
  else {
    return ( $field_label ? $field_label . ': ' : '' ) . $label;
  }
}

/**
 * Override the xc_search_info_bar_back_to_results theme
 * 
 * @param $meta (Object)
 *   The meta object, which contains information about the search result
 */
function xc_default_xc_search_info_bar_back_to_results($meta) {
  $results_page = $meta->extra['list'];
  
  // get image
  $image = theme('image', 
    drupal_get_path('theme', 'xc_theme') .'/xc_default/images/arrow-back.png', 
    $results_page['label'], $results_page['label']
  );
  
  // link options
  $options = array(
    'html' => TRUE, 
    'query' => $results_page['query'], 
    'fragment' => $results_page['fragment']
  );
  
  $output = '<div class="xc-search-back-to-results">'
    . l($image, $results_page['url'], $options)
    . ' '
    . l($results_page['label'], $results_page['url'], $options)
    . '</div>';
  return $output;
}

/**
 * Theming the auto login form
 * 
 * @param $form (Array)
 *   The form object
 */
function xc_default_xc_auth_auto_login_form($form) {
  $output = '';
  foreach (element_children($form) as $id => $key) {
    if ($key == 'submit') {
      $submit = drupal_render($form[$key]);
    }
    else if ($key == 'reset_password_text') {
      $text = drupal_render($form[$key]);
    }
    else {
      $output .= drupal_render($form[$key]);
    }
  }
  $output .= '<div id="xc-login-text-and-button">'
    . '<span id="xc-login-button">' . $submit . '</span>'
    . $text
    . '</div>';
  $output .= drupal_render($form);
  return $output;
}
