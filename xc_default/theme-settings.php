<?php

include_once './' . drupal_get_path('theme', 'xc_theme') . '/theme-settings.php';

function xc_default_settings($saved_settings) {
  $subtheme_defaults = array();

  return xc_theme_settings($saved_settings, $subtheme_defaults);
}